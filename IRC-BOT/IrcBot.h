/*
 * IrcBot.h
 *
 *      Author: Alessandro Cecchin AKA Acn0w
 */

#ifndef IRCBOT_H_
#define IRCBOT_H_

class IrcBot
{
public:
	IrcBot(char * _nick, char * _usr);
	virtual ~IrcBot();

	bool setup;

	void start();
	bool charSearch(const char *toSearch, const char *searchFor);

private:
	const char *port;
	int s; //the socket descriptor

	char *nick;
	char *usr;

	bool isConnected(const char * buf);

	char * timeNow();

	bool sendData(const char *msg);

	void sendPong(const char *buf);

	void msgHandel(const char *buf);
};

#endif /* IRCBOT_H_ */
