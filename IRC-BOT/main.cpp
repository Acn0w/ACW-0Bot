//Author: Alessandro Cecchin AKA Acn0w
#include <iostream>
#include "IrcBot.h"


using namespace std;


int main()
{
	char nick[] = "NICK ACW-0\r\n";
	char user[] = "USER guest 0 * :Acn0w's Bot\r\n";//see IRC 2812 for more info https://tools.ietf.org/html/rfc2812#
	IrcBot bot = IrcBot(nick,user);
	bot.start();

  return 0;

}
