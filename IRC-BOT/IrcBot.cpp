/*
 * IrcBot.cpp
 *
 *      Author: Alessandro Cecchin AKA Acn0w
 


To change the server you wish the IRC bot to connect to change “irc.ubuntu.com” to the desired server at line 81.

You can change which channel you the IRC bot to connect to by changing “#ubuntu” at line 123 to the desired channel.

The msgHandel() function at line 290 is where you’ll add your message handling code, at the moment the bot will reply 
to any command with the words “hi ACW-0” it with the reply “Hi, hows it going”. To add more simply copy the if 
statement at line 293 and substitute the “hi ACW” for the command. Then change “hi, hows it going” to the required reply. 
Remember that all messages sent must finish with “\r\n” otherwise the server will wait for more commands.

For more information on the messages used in the IRC Protocol goto http://www.irchelp.org/irchelp/rfc/chapter4.html.
*/




#include "IrcBot.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <time.h>

using namespace std;

#define MAXDATASIZE 100


IrcBot::IrcBot(char * _nick, char * _usr)//Costructor of the object IrcBot
{
	nick = _nick;
	usr = _usr;
}

IrcBot::~IrcBot()//Destroyer of the object IrcBot
{
	close (s);
}

void IrcBot::start()//Initialize and start the Bot
{
	struct addrinfo hints, *servinfo;
	

	//Setup run with no errors
	setup = true;

	port = "6667";

	//Ensure that servinfo is clear
	memset(&hints, 0, sizeof hints); // make sure the struct is empty

	//setup hints
	hints.ai_family = AF_UNSPEC; // don't care IPv4 or IPv6
	hints.ai_socktype = SOCK_STREAM; // TCP stream sockets

	//Setup the structs if error print why
	int res;
	if ((res = getaddrinfo("irc.ubuntu.com",port,&hints,&servinfo)) != 0)
	{
		setup = false;
		fprintf(stderr,"getaddrinfo: %s\n", gai_strerror(res));
	}


	//setup the socket
	if ((s = socket(servinfo->ai_family,servinfo->ai_socktype,servinfo->ai_protocol)) == -1)
	{
		perror("client: socket");
	}

	//Connect
	if (connect(s,servinfo->ai_addr, servinfo->ai_addrlen) == -1)
	{
		close (s);
		perror("Client Connect");
	}


	//We dont need this anymore
	freeaddrinfo(servinfo);


	//Recv some data
	int numbytes;
	char buf[MAXDATASIZE];

	int count = 0;
	while (1)
	{
		//declars
		count++;

		switch (count) {
			case 3:
					//after 3 recives send data to server (as per IRC protacol)
					sendData(nick);
					sendData(usr);
				break;
			case 4:
					//Join a channel after we connect, this time we choose beaker
				sendData("JOIN #Acn0w\r\n");
			default:
				break;
		}



		//Recv & print Data
		numbytes = recv(s,buf,MAXDATASIZE-1,0);
		buf[numbytes]='\0';
		cout << buf;
		//buf is the data that is recived

		//Pass buf to the message handeler
		msgHandel(buf);


		//If Ping Recived
		/*
		 * must reply to ping overwise connection will be closed
		 * see http://www.irchelp.org/irchelp/rfc/chapter4.html
		 */
		

		if (charSearch(buf,"PING"))
		{
			sendPong(buf);
		}

		//break if connection closed
		if (numbytes==0)
		{
			cout << "----------------------CONNECTION CLOSED---------------------------"<< endl;
			cout << "(Ping timeout)" << endl;
			cout << timeNow() << endl;

			break;
		}
	}
}


bool IrcBot::charSearch(const char *toSearch, const char *searchFor)//Search for commands
{
	
	int len = strlen(toSearch);//The length of the buf in what we search
		int forLen = strlen(searchFor); // The length of the searchfor field

		//Search through each char in toSearch
		for (int i = 0; i < len; i++)
		{
			//If the active char is equal to the first search item then search toSearch
			if (searchFor[0] == toSearch[i])
			{
				bool found = true;
				//search the char array for search field
				for (int x = 1; x < forLen; x++)
				{
					if (toSearch[i+x]!=searchFor[x])
					{
						found = false;
					}
				}
	
				//if found return true;
				if (found == true)
					return true;
			}
		}

	return 0;
	
}



bool IrcBot::isConnected(const char *buf)
{//returns true if "/MOTD" is found in the input string
	//If we find /MOTD then its ok join a channel
	if (charSearch(buf,"/MOTD") == true)
		return true;
	else
		return false;
}

char * IrcBot::timeNow()
{//returns the current date and time
	time_t rawtime;
	struct tm * timeinfo;

	time ( &rawtime );
	timeinfo = localtime ( &rawtime );

	return asctime (timeinfo);
}


bool IrcBot::sendData(const char *msg)
{       //Send some data
	int len = strlen(msg);
	int bytes_sent = send(s,msg,len,0);

	if (bytes_sent == 0)
		return false;
	else
		return true;
}

void IrcBot::sendPong(const char *buf)
{
	//Get the reply address
	//loop through bug and find the location of PING
	//Search through each char in toSearch
	const char * toSearch = "PING";//Load "PING " into the array toSearch

	for (int i = 0; i < strlen(buf);i++)
		{
			//If the active char is equal to the first search item then search toSearch
			if (buf[i] == toSearch[0])
			{
				bool found = true;
				//search the char array for search field
				for (int x = 1; x < 4; x++)
				{
					if (buf[i+x]!=toSearch[x])
					{
						found = false;
					}
				}

				//if found return true;
				if (found == true)
				{
					int count = 0;
					//Count the chars
					for (int x = (i+strlen(toSearch)); x < strlen(buf);x++)
					{
						count++;
					}

					//Create the new char array with the PONG string
					char returnHost[count + 5];
					returnHost[0]='P';
					returnHost[1]='O';
					returnHost[2]='N';
					returnHost[3]='G';
					returnHost[4]=' ';


					count = 0;
					//set the hostname data
					for (int x = (i+strlen(toSearch)); x < strlen(buf);x++)
					{
						returnHost[count+5]=buf[x];
						count++;
					}

					//send the pong
					{
					if (sendData(returnHost))
						cout << timeNow() <<"  Ping Pong" << endl;
					}


					return;
				}
			}
		}

}

void IrcBot::msgHandel(const char * buf)//This function parse commands given from the chat
{
	if (charSearch(buf,"*"))
	{
		if(charSearch(buf,"hello"))
			sendData("PRIVMSG #Acn0w :hi, hows it going?\r\n");
		

		/*if (charSearch(buf,"time"))
			time_t rawtime;
  			struct tm * timeinfo;

  			time(&rawtime);
  			timeinfo = localtime (&rawtime);
  			string string = asctime(timeinfo);
			sendData("PRIVMSG #Acn0w :Current local time and date: %s" + string);
			*/
	}
}

