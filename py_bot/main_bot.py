#Author: Alessandro Cecchin AKA Acn0w 
import telegram
from telegram.ext import Updater
import logging
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters
from telegram.ext import InlineQueryHandler
from telegram import InlineQueryResultArticle, InputTextMessageContent
import subprocess


with open('../../token_bot_telegram.txt', 'r') as f:
    token = f.readline().rstrip()
#print (token)

bot = telegram.Bot(token)

#show if the bot is ok
#print(bot.get_me())

#create an Updater object
# Its purpose is to receive the updates from Telegram and to deliver them to said
#Dispatcher. It also runs in a separate thread
updater = Updater(token, use_context=True)

#For quicker access to the Dispatcher used by your Updater
dispatcher = updater.dispatcher

# If a handler raises an uncaught exception that is no TelegramError (e.g. an IndexError),
# the exception will be caught and logged by the Dispatcher, so that the bot does
# not crash but you still have an indication of it and can address the issue. To
# take advantage of this, it is imperative to set up the logging module.

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.INFO)

#define a function that should process a specific type of update
def start(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text="At your service, Chief!")

    # The goal is to have this function called every time the Bot receives a
    # Telegram message that contains the /start command. To accomplish that, you
    # can use a CommandHandler (one of the provided Handler subclasses) and
    # register it in the dispatcher:
start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)

################### VIDEO DOWNLOAD##############################################
# Download video
def video(update, context):
    url = context.args
    if(len(url)==1):
        context.bot.send_message(chat_id=update.effective_chat.id, text="Download STARTED")
        subprocess.check_output(["youtube-dl", url[0]])
        context.bot.send_message(chat_id=update.effective_chat.id, text = "Download of the file '" + subprocess.check_output(["youtube-dl", "--get-title", url[0]]).decode('UTF-8') + "' COMPLETED")
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text = "Error! Use format like /video https://.. And I'll downlaod the best possible video ")


video_handler = CommandHandler('video', video)
dispatcher.add_handler(video_handler)

############################ IN LINE ###########################################
#inline command that turn the message in all caps
def inline_caps(update, context):
    query = update.inline_query.query
    if not query:
        return
    results = list()
    results.append(
        InlineQueryResultArticle(
            id=query.upper(),
            title='Caps',
            input_message_content=InputTextMessageContent(query.upper())
        )
    )
    context.bot.answer_inline_query(update.inline_query.id, results)

inline_caps_handler = InlineQueryHandler(inline_caps)
dispatcher.add_handler(inline_caps_handler)

#################### UNKNOWN ###################################################
#all command that aren't defined will be ignored
def unknown(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text="Sorry, I'm not smart enough for that'.")

unknown_handler = MessageHandler(Filters.command, unknown)
dispatcher.add_handler(unknown_handler)

################### STOP #######################################################
def stop(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text="See you soon!")
    updater.stop()

unknown_handler = CommandHandler('stop', unknown)
dispatcher.add_handler(unknown_handler)







updater.start_polling()
